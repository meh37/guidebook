# A human-friendly name for the site as a whole.
site_name: "DevOps Division Guidebook"

# The canonical URL of the site.
site_url: "https://guidebook.devops.uis.cam.ac.uk/"

# Site layout
nav:
  - index.md
  - "Contact us":
      - contact/index.md
      - contact/public-keys.md
      - guidebook.md
  - "How-to":
      - howtos/index.md
      - "Git/GitLab":
          - howtos/sign-in-to-gitlab.md
          - howtos/open-an-issue.md
          - howtos/size-issues.md
          - howtos/create-a-feature-branch.md
          - howtos/add-the-mit-licence.md
          - howtos/common-git-tasks.md
      - "Development":
          - howtos/prepare-your-system.md
          - howtos/create-a-new-app.md
          - howtos/run-an-existing-app.md
          - howtos/configure-your-ide.md
          - howtos/run-database-migrations-when-deploying.md
      - "GKE GitLab Runners":
          - howtos/gke-gitlab-runners/register-a-gke-gitlab-runner.md
          - howtos/gke-gitlab-runners/run-ci-jobs-on-a-gke-gitlab-runner.md
          - howtos/gke-gitlab-runners/access-secrets-in-ci-jobs-using-impersonation.md
      - "Python":
          - howtos/publish-a-python-package.md
          - howtos/add-common-pipeline-to-python.md
          - howtos/pypi.md
  - "Learn":
      - "Tutorials":
          - tutorials/index.md
          - tutorials/creating-a-python-package.md
          - tutorials/bootstrapping-a-new-application.md
      - "Explanations":
          - explanations/index.md
          - explanations/identifiers.md
          - explanations/webapp-boilerplate.md
          - "Git/GitLab":
              - explanations/good-issues.md
              - explanations/reviewing-issues.md
              - explanations/git.md
          - explanations/gke-gitlab-runners.md
          - "Python":
              - workflow/pypi.md
  - "Reference":
      - reference/index.md
      - external-docs.md
      - reference/identifiers.md
      - reference/webapp-developer-environment.md
      - "Git/GitLab":
          - "Labels": reference/gitlab-labels.md
          - "Python CI jobs": "https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/python.md"
          - reference/git-docs.md
      - "Best Practice":
          - best-practice/index.md
          - best-practice/api.md
          - best-practice/javascript.md
          - best-practice/python.md
          - best-practice/webapps.md
          - best-practice/ourtools.md
      - "Cloud Platform":
          - reference/cloud-platform/index.md
          - reference/cloud-platform/gcp-folders.md
          - reference/cloud-platform/dns.md
          - reference/cloud-platform/permissions-and-roles.md
          - reference/cloud-platform/backups.md
          - reference/cloud-platform/gke-gitlab-runners.md
      - "App Deployment":
          - deployment/index.md
          - deployment/terraform.md
          - deployment/secrets.md
          - deployment/sql-instances.md
          - deployment/web-applications.md
          - deployment/traffic-ingress.md
          - deployment/k8s-clusters.md
          - deployment/monitoring-and-alerting.md
          - deployment/continuous-deployment.md
          - deployment/templates.md
      - "Working Environment":
          - environment/culture.md
          - environment/meeting-rooms.md
      - "Glossary": "glossary.md"
      - "Workflow":
          - workflow/index.md
          - workflow/gitlab.md
          - workflow/flows.md
          - workflow/merge-requests.md
          - workflow/pipelines.md
          - workflow/onboarding.md
          - workflow/credentials-and-secrets.md
          - workflow/git-tricks.md
      - "Notes":
          - notes/index.md
          - notes/webapp-dev-environment.md
          - notes/automating-google-drive.md
          - notes/gcp-deployments.md
          - notes/google-domain-verification.md
          - notes/gunicorn-tuning.md
          - notes/ucs-dev-group-services.md
          - notes/internal-ca.md
          - notes/raven-test-sites.md
          - notes/faq.md
  - "Our Services":
      - services/index.md
      - "Education":
          - "Digital Admissions":
              - services/my-cambridge-application.md
              - services/subject-moderation-interface.md
          - services/events.md
          - services/essm-sync.md
          - services/job-opportunities.md
          - "Laboratory Allocation":
              - services/lab-allocator.md
              - services/legacy-nst-ia-labs.md
          - "Lecture Capture":
              - services/panopto-deployment.md
              - services/lecture-capture-preferences.md
          - services/streaming-media.md
          - services/yuja.md
          - services/utbs.md
          - services/ada.md
          - services/panel.md
          - services/governance.md
      - "Infrastructure and Generic Services":
          - services/api-gateway.md
          - services/gaobase.md
          - services/gitlab.md
          - services/equipshare.md
          - services/ups-service-definition.md
          - "Identity and Access Management":
              - services/iam-overview.md
              - services/eduroam-tokens.md
              - "Card Management":
                  - services/legacy-card-db.md
                  - services/card-management-system.md
              - services/gsuite.md
              - services/gsuite-preferences.md
              - services/jackdaw.md
              - services/lookup.md
              - "Raven":
                  - services/raven-metadata.md
                  - services/raven-oauth.md
                  - services/passwords.md
                  - services/raven-shibboleth.md
                  - services/raven-ucamwebauth.md
                  - services/raven-stats.md
                  - services/raven-saml2-shim.md
              - services/university-human-resources-api.md
              - services/university-student-api.md
          - services/information-asset-register.md
          - services/self-service-gateway.md
          - services/design-system.md
          - services/talks-cam.md
          - services/nursery-waiting-list.md
          - services/scribe.md
          - services/regent-house-ballots.md
      - "Research":
          - services/research-dashboard.md
          - services/hta.md
          - services/ronin.md
          - services/tas.md
          - services/staff-information-system.md
          - services/TOAST.md
          - services/gates-trust.md
          - services/interfacer.md
          - services/roo-intranet.md
          - services/budget-calculator.md
          - services/claims-management-system.md
          - services/rco-database.md
          - services/sissp.md
      - "Retired Services":
          - services/covid-pooled-testing.md
          - services/software-sales.md
          - services/tls-certificates.md

# Links for sufficiently with it people to submit changes.
repo_url: "https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/"
edit_uri: "blob/master/docs"
repo_name: "Developer Hub Project"

# Make us look pretty.
theme:
  name: "material"
  font:
    text: "Lato"
  features:
    - navigation.tracking
    - navigation.top
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.indexes
    - navigation.sections
    - toc.integrate
    - content.code.copy
  palette:
    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: "default"
      primary: "teal"
      secondary: "teal"
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: "slate"
      primary: "teal"
      secondary: "teal"
      toggle:
        icon: material/brightness-4
        name: Switch to light mode

markdown_extensions:
  # Support notes, warnings, etc.
  - admonition

  # Allow the use of Pygments to do code highlighting. Do not attempt to guess
  # the language if we don't specify.
  - codehilite:
      guess_lang: false

  # Provide permalinks to help with linking to sections.
  - toc:
      permalink: true

  # Support GitLab/GitHub-style task lists.
  - pymdownx.tasklist:
      custom_checkbox: true

  # Allow code blocks to be nested inside other elements
  - pymdownx.superfences

  # Allow Markdown to be used within HTML elements with the "markdown" attribute set.
  - md_in_html

  # Definition lists
  - def_list

  # Tables
  - tables

  # Inline icons and emoji
  - attr_list
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg


  - pymdownx.details

  # Allow for inline highlighting of code blocks.
  - pymdownx.highlight
  - pymdownx.inlinehilite

  # Footnotes for the Pterry-inclined.
  - footnotes

  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format

extra_css:
  - stylesheets/gitlab-labels.css
  - stylesheets/code-block.css

extra_javascript:
  - https://unpkg.com/tablesort@5.3.0/dist/tablesort.min.js
  - javascripts/tablesort.js

plugins:
  - search
  - macros
  - redirects:
      redirect_maps:
        "best-practice/git.md": "explanations/git.md"
        "deployment/gcp-folders.md": "reference/cloud-platform/gcp-folders.md"
        "deployment/permissions-and-roles.md": "reference/cloud-platform/permissions-and-roles.md"
        "deployment/backups.md": "reference/cloud-platform/backups.md"
        "deployment/dns.md": "reference/cloud-platform/dns.md"
