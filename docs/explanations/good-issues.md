# Writing good issues

We use GitLab issues to capture individual work items.

Imagine you are coming to pick up a new issue. In order to be able to proceed
you need to at a minimum know:

* the current state of the product as it relates to the issue;
* what needs to be done to address the issue;
* *why* we have chosen to address the issue that way;
* how a reviewer will know if an issue is addressed; and
* what discussion has taken place before the issue came into the sprint.

Issue writing is in many ways more of an art than a science and is something
which you will have to learn by experience. As a rule of thumb, always write
the issue *you* would want to pick up if assigned.

We have [issue templates](https://gitlab.developers.cam.ac.uk/uis/devops)
designed to ensure that the description of an issue and the subsequent
discussion ensures that someone picking it up knows everything outlined above.
These templates have been developed in response to real-world experience and so
should be used if at all possible when creating new issues.

It is OK for an issue to be opened if there is no current consensus on how it
can be addressed. Indeed, discussion on how to proceed with an issue is
valuable. If you are unsure as to how an issue should be addressed, make sure
it is labelled as {{ gitlab_label("workflow::Needs Refinement") }}.

A good issue description should both capture the issue at the time it was
opened *and* the state of the issue post discussion. It is far easier to get a
handle on an issue if any discussion in the comments of the issue has been
summarised in the description. If there has been extensive discussion in the
issue, the description should capture the *consensus view* on how to proceed
but need not capture the in-depth reasoning; anyone interested in that can read
the discussion.

If discussion has happened outside of GitLab, either include a link to the
discussion or provide a summary of the discussion and who was present. This
makes finding the correct people to ask follow-up questions to far easier.

## Next steps

* Simon Tatham's [How to Report Bugs
  Effectively](https://www.chiark.greenend.org.uk/~sgtatham/bugs.html) is
  useful reading if you are opening issues relating to bugs or missing
  features.
