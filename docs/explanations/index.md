# Explanations

This section of the guidebook is where we describe the *why* of what we do. It
provides a location where technical discussions can be summarised and the
rationale for decisions recorded.
