---
title: Glossary
---
Glossary of terms relevant to the DevOps team and helpful for new starters:

* **Cambridge Student Information System (CamSIS)** : The University's comprehensive system for handling student information.
* **Cambridge Human Resources Information System (CHRIS)** : The University's Human Resources (HR) system.  
* **HEAT** : The University Information Services (UIS) support management system.
* **Jackdaw** : The University Information Services (UIS) administrative database.
* **Lookup** : a directory containing information about people known to Cambridge University and its affiliate organisations.
* **Raven** : The University of Cambridge's central web authentication service.
* **Universities and Colleges Admissions Service (UCAS)** : a charity connecting people to higher education.
