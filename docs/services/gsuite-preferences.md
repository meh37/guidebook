# Google Workspace Preferences App

This page documents key information about the Google Workspace preferences app.

The preferences app allows any member of the University with a Raven account to
opt in to additional Google services not covered by the core Terms and
Conditions associated with our Google Workspace license.

Behind the scenes it manages membership of multiple "opt in" Google Groups which
each have one or more additional services enabled for them.

## Environments

- Production at https://preferences.g.apps.cam.ac.uk/
- Staging at https://preferences.test.gworkspace.gcp.uis.cam.ac.uk/
- Development at https://preferences.devel.gworkspace.gcp.uis.cam.ac.uk/

## Application repositories

- [Web
    application](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/preferences-webapp)
- [Terraform
    deployment](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/deploy)

## Deployment

Deployed via terraform using our usual deployment boilerplate.

## Current Status

Live

## Service Management and tech lead

The service owner for the service is currently unknown.

The **service manager** for the service is [Dr Я
Charles](https://www.lookup.cam.ac.uk/person/crsid/rc118).

The **tech lead** for the service is vacant.

The following engineers have operational experience are able to respond to support requests or incidents:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg11)
