# SCRIBE

!!! warning
    This page incomplete. Please contact [uis-cloud@lists.cam.ac.uk](mailto:uis-cloud@lists.cam.ac.uk) for more information

This page gives an overview of SCRIBE, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

SCRIBE is an application to manage legal caseloads for the Legal Office

## Service Status

The SCRIBE is currently **live**.

<!-- `<notes about status and links to roadmaps / timelines for future development or decommissioning>` -->

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be picked up
by a member of the team working on the service.

<!-- Issues discovered in the service or new feature requests should be opened as 
`[GitLab issues in the application repository](<link to gitlab issues page>)`.-->

<!-- ## Environments

The SCRIBE is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | `<production_url>` | `<list of vms>` |
| Staging     | `<staging_url>`    | `<list of vms>` |
| Development | `<dev_url>`        | `<list of vms>` |

`<notes about access to environments and anything special about how an environment is used>`
-->
<!-- ## Source code

The source code for the SCRIBE is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server]() | The source code for the main application server |
| [Infrastructure Deployment]() | The Terraform infrastructure code for deploying the application server to GCP |
-->

## Technologies used

The following gives an overview of the technologies the SCRIBE is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | IIS | VB.NET |
| Client | IIS | VB.NET |

<!-- ## Operational documentation

The following gives an overview of how the SCRIBE is deployed and maintained.

### How and where the SCRIBE is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and locally>`

### `<other operation issues>`

`<documentation or links to external docs about other operation issues>`
-->

## Service Management and tech lead

The **service owner** for the SCRIBE is **TBA**.

The **service manager** for the SCRIBE is **TBA**.

The **tech lead** for the SCRIBE is [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37).

<!-- The following engineers have operational experience with the SCRIBE and are able to
respond to support requests or incidents:

* `<engineer name and Lookup link>`
* `<engineer name and Lookup link>`
-->