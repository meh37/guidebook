# Legacy application backups

The (ex UCS devteam) legacy applications managed by DevOps have three types of backups:

* Veeam backups

* Moa database backups (both managed by Servers and Storage)

* local database dumps


## Veeam backups

Veeam backups can be utilised in a number of ways.

The three levels of backup are:

* VM (Machine Level)

* File Level

* Application Level


Backups are taken at a point-in-time set on a backup schedule.  The backups are made up of Full Backups and then subsequent Incremental backups.

These restore-points are then kept for as long as dictated by the retention policy on the backup job.  E.g.  Keep for 30 days or 30 restore points.

The restore process is currently something only the Servers and Storage Team can do for you.

### Restore options:

#### Restore VM

* Restore the entire VM and overwrite the live data.

* Restore the entire VM and stage the restore (known as Instant Restore) before “publishing” this as live (the restored VM can be staged without being connected to the network)

* Restore the entire VM and stage this in a sandbox (known as a Virtual Lab.  Current works but VERY slow due to the slow disk hardware)
With restoring a VM, we can also make changes to the VM before it is restored with a new name, change of IP, reconfiguring of hardware etc.

* Restore individual disks or files from backup (known as File Level Restore – FLR).
The virtual disk images (VMDK) can be restored and attached to another VM.
Individual files can be restored back to the original location or to a specified location.

#### Restore application level data:

If the backup job has been made application aware, it can restore data directly from within the application database.  The data can be explored then exported and then restored or imported into a target location.
To do application aware backups and restores, Veeam leverages specific Explorer applications for specific products.


The current list of Explorers are:

* Veeam Explorer for Microsoft Exchange

* Veeam Explorer for Microsoft SharePoint

* Veeam Explorer for Microsoft OneDrive for Business

* Veeam Explorer for Microsoft Teams

* Veeam Explorer for Microsoft Active Directory

* Veeam Explorer for Microsoft SQL Server

* Veeam Explorer for Oracle

* Veeam Explorer for PostgreSQL

The database backup process can be configured to backup and restore with a number of options.   E.g.  Full or Quick restore, transaction log backup, restore to source or new target etc
It would be useful to make sure the correct options are selected for your specific backups and that the restore process is documented as part of your data recovery plan.

Requests for file restore should be made to submitted to the servicedesk servicedesk@uis.cam.ac.uk which will create a ticket in the servicedesk system so we can track the progress.

The information we would need is the name of the VM, level of restore, object to be restored, type of restore and date to restore from.  The more information you can provide will give us the best chance of locating and restoring the data you need.


### The Moa postges service

Database backups are also taken by the Servers and Storage team that runs the Moa service. These backups include database data and configuration files. These are intended for Servers and Storage team disaster recovery purposes, but they are willing to deal with occasional restore requests. Because this uses pg_basebackup, they can only restore an entire cluster at once. Restores can be requested by emailing infra-sas@uis.cam.ac.uk.


### Local database backups

Database backups are taken at regular intervals on the VMs themselves. Backup and restore scripts can be found in /usr/share/<app_name>/bin/<app_name>-[backup | restore].

These can be used to make adhoc backups by running <app_name>-backup now (results in a bzipped database dump in the current directory).

Cronjobs run these script at regular intervals and the resulting bzipped backups are stored in /usr/share/<app_name>/backup.