# Job Opportunities

This page gives an overview of the Job Opportunities, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

Welcome to the University of Cambridge's Job Opportunities pages. Here you can view our current vacancies and those of many Cambridge colleges and affiliated institutions. You can also find all of the information you need about working for one of the world's oldest and most successful universities.

## Service Status

The Job Opportunities is currently live.

## Contact

The primary contacts for the Job Opportunities are [Simon Virr](https://www.lookup.cam.ac.uk/person/crsid/sav25) and [Andrew Rowland](https://www.lookup.cam.ac.uk/person/crsid/ar972).

The service manager is [Andrew Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322)

Issues discovered in the service or new feature requests should be opened as [GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/job-oppurtunities-website/job-opportunities).

## Environments

The Job Opportunities is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [http://www.jobs.cam.ac.uk/](http://www.jobs.cam.ac.uk/) | silver, ellington |
| Staging     | [http://staging.jobs.internal.admin.cam.ac.uk/](http://staging.jobs.internal.admin.cam.ac.uk/)   |jackson|
| Development | [http://dev.jobs.internal.admin.cam.ac.uk/](http://dev.jobs.internal.admin.cam.ac.uk/) | rich |

## Source code

The source code for the Job Opportunities is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [job-opportunities](https://gitlab.developers.cam.ac.uk/uis/devops/job-oppurtunities-website/job-opportunities) | Application Server |

## Technologies used

The following gives an overview of the technologies the Job Opportunities is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Application |PHP | REST |

### Monitoring

App logs to `/home/httpd/www.jobs.cam.ac.uk/logs`.

Apache logs to usual location (currently `/var/log/httpd`).

## Service Management and tech lead

The **service owner** for the Job Opportunities is [Liz Timperley-Preece](https://www.lookup.cam.ac.uk/person/crsid/et305) (HR).

The **service manager** for the Job Opportunities is [Andrew Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322)

The **tech lead** for the Job Opportunities is TBD

The following engineers have operational experience with the Job Opportunities and are able to
respond to support requests or incidents:

* engineer name TBD
* engineer name TBD
