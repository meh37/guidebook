# Governance

This page gives an overview of the Governance portal, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

A central resource for governance information, [the governance site] (https://www.governance.cam.ac.uk/) details the University's governance structure and decision-making processes, and provides access to the work of the Council and central committees.​

## Service Status

The governance site is currently live.


## Contact

Technical queries and support should be directed to `<role email>` and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to `<role email>` rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/dotnet/governance).

## Environments

The governance site is currently deployed to the following SharePoint farms:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | https://www.governance.cam.ac.uk/ |  |
|             | Web Server         | spt-live-web1.internal.admin.cam.ac.uk |
|             | Application Server | spt-live-app1.internal.admin.cam.ac.uk |
|             | Database           | spt-live-db1.internal.admin.cam.ac.uk |
| Staging     | https://spt-test.admin.cam.ac.uk/governance_test  |  |
|             | Web Server         | spt-test-web1.internal.admin.cam.ac.uk |
|             | Application Server | spt-test-app1.internal.admin.cam.ac.uk |
|             | Database           | spt-test-db1.internal.admin.cam.ac.uk |



## Source code

The source code for the governance portal is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [SharePoint 2010 release](https://gitlab.developers.cam.ac.uk/uis/dotnet/governance/clearpeople) | The source code by Clear People |
| [SharePoint 2010 update](https://gitlab.developers.cam.ac.uk/uis/dotnet/governance/sp2010) | The patches developed  after original release |


## Technologies used

The following gives an overview of the technologies the governance portal is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| SharePoint 2010 portal | C#, PowerShell | SharePoint 2010 server solution |
| SharePoint 2013 portal | C#, PowerShell | SharePoint 2013 server solution |



## Operational documentation

The following gives an overview of how the governance portal is deployed and maintained.

### How and where the governance portal is deployed

The SharePoint server side features are deployed after a SharePoint publishing site is created.

### Deploying a new release

There is no more active development. The server-side code are deployed using PowerShell scripts running on SharePoint server.


### Debugging

There is no more active development.



## Service Management and tech lead

The **service owner** for the Governance portal is `<service owner name and Lookup link>`.

The **service manager** for the Governance portal is `<service manager name and Lookup link>`.

The **tech lead** for the Governance portal is `<tech lead name and Lookup link>`.

The following engineers have operational experience with the governance portal and are able to
respond to support requests or incidents:

* [Dean Johnson](https://www.lookup.cam.ac.uk/person/crsid/dj257) - Dean has moved to other division in UIS and is not known to be able to provide any service to the governance portal.
* [Jie Lin](https://www.lookup.cam.ac.uk/person/crsid/jl364)
