title: UTBS

# University Training Booking System

This page gives an overview of the University Training Booking System (UTBS), describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description
The UTBS provides an admin and booking system for training events to various University institutions.

## Service Status

The UTBS is currently live.

## Contact

Technical queries and support should be directed to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always direct requests to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs/-/issues).

## Environments

The UTBS is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://training.cam.ac.uk](https://training.cam.ac.uk)           | utbs-live1.srv.uis.private.cam.ac.uk |
|             | [https://uis-tm-live.training.cam.ac.uk/](https://uis-tm-live.training.cam.ac.uk/) | utbs-live2.srv.uis.private.cam.ac.uk |
|             |                                      | utbs-live-db.srv.uis.private.cam.ac.uk |
| Training    | [https://training.training.cam.ac.uk](https://training.training.cam.ac.uk)           | utbs-train1.srv.uis.private.cam.ac.uk |
|             | [https://uis-tm-train.training.cam.ac.uk/](https://uis-tm-train.training.cam.ac.uk/) | utbs-train2.srv.uis.private.cam.ac.uk |
|             |                                      | devgroup-test-db.srv.uis.private.cam.ac.uk |
| Staging     | [https://uis-tm-test.training.cam.ac.uk/](https://uis-tm-test.training.cam.ac.uk/)      | utbs-test1.srv.uis.private.cam.ac.uk |
|             |                                      | utbs-test2.srv.uis.private.cam.ac.uk |
|             |                                      | devgroup-test-db.srv.uis.private.cam.ac.uk |

The training environment is used to train users of the system.

## Source code

The source code for the UTBS is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs) | The source code for the main application server. |
| [UTBS API client](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs-client)|utbs-client provides Java, Python and PHP client code for accessing the UTBS web service API.|
| [Ansible](https://gitlab.developers.cam.ac.uk/uis/devops/grails-application-ansible-deployment) | The Ansible used to deploy all the Dev Group Grails Apps |

## Technologies used

The following gives an overview of the technologies the UTBS is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web Application | Groovy 2.4.12 | Grails 1.3.10 |
| DB | Postgres | 13 |

## Operational documentation

The following gives an overview of how the UTBS is deployed and maintained.

### How and where the UTBS is deployed

### Deploying a new release

The UTBS application is deployed using WAR packages. These are built by Gitlab CI in the [Application Server repository](https://gitlab.developers.cam.ac.uk/uis/devops/utbs/utbs). These are deployed using the Ansible above

### Monitoring

The UTBS is monitored by the UIS infra-sas [nagios service](https://nagios.uis.cam.ac.uk/nagios/).

Sevices currently monitored:

* ping - standard nagios ping check.
* SSL - checks for a valid TLS certificate on port 8443.
* https_devgroup - checks for a 200 response from the /adm/status page on port 8443.
* disc-space - checks for at least 15% free disk space.
* colocation-check - checks that all resources are located in distinct locations.

There is also a check for a vaild TLS certificate being served by the traffic manager for https://uis-tm-live.training.cam.ac.uk/


### Backups
See [legacy application backups](legacy-application-backups.md)


### Debugging

A docker development environment is available.

## Service Management and tech lead

The **service owner** for the UTBS is [Monica Gonzalez](https://www.lookup.cam.ac.uk/person/crsid/mg356).

The **service manager** for the UTBS is [Monica Gonzalez](https://www.lookup.cam.ac.uk/person/crsid/mg356).

The **tech lead** for the UTBS is  [Paul Badcock](https://www.lookup.cam.ac.uk/person/crsid/prb34).

The following engineers have operational experience with the UTBS and are able to respond to support requests or incidents:

* [Steve Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
* [Adam Deacon](https://www.lookup.cam.ac.uk/person/crsid/ad2139)


System Dependencies
-------------------
- [Lookup](lookup.md)
- [Raven](raven-ucamwebauth.md)

Contact Addresses
-----------------
- trainingservices@uis.cam.ac.uk
