# Staff Information System

!!! warning
    This page incomplete. Please contact [uis-cloud@lists.cam.ac.uk](mailto:uis-cloud@lists.cam.ac.uk) for more information

This page gives an overview of the Staff Information Service, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The Staff Information System is an application for THe Research Information Office. It works with CHRIS data
and SharePoint front end to support the RIO.

## Service Status

The Staff Information Service is currently **live**.

## Contact

Technical queries and support should be directed to `uis-cloud@lists.cam.ac.uk` and will be picked up
by a member of the team working on the service.

## Environments

The Staff Information System is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | TBA | TBA |
| Staging     | TBA | TBA |
| Development | TBA | TBA |

<!--## Source code

The source code for the Staff Information System is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server]() | The source code for the main application server |
-->
## Technologies used

The following gives an overview of the technologies the Staff Information System is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | C++ | TBA |

## Operational documentation

The following gives an overview of how the Staff Information System is deployed and maintained.

### How and where the Staff Information System is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

## Service Management and tech lead

The **service owner** for the Staff Information System is TBA.

The **service manager** for the Staff Information System is [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37)

The **tech lead** for the Staff Information System is [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37)
