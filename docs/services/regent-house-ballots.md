# Regent House Ballots application

This page gives an overview of the Regent House ballots application, describing its current status,
where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

The [Regent
House](https://www.governance.cam.ac.uk/governance/key-bodies/rh-senate/pages/regent-house-membership-and-rights.aspx)
is a key governance body for the University. Membership of the Region House will vote in *Ballots of
the Regent House*. These ballots cover a wide range of issues facing the Regent House.

The University out-sources the actual voting recording and tallying to an external third-party. All
ballots have a list of eligible voters. For each voter on a particular ballot, the third-party
provider gives the University a unique URL which the voter can use to vote on a ballot.

This web application presents a single shareable URL representing a ballot. When a University member
visits that URL, they are authenticated and, should they be on the voter list for that ballot, they
are redirected to their unique voting URL.

Additionally, this web application provides a management interface which allows administrators to
create ballots, specify the times at which they open and close and upload a list of voters along
with unique voter URLs.

## Service Status

The Region House Ballots app is currently in early user testing.

## Contact

Technical queries and support should be directed as per our [contact page](../contact/index.md).

Issues discovered in the _functionality_ of service or new feature requests should be opened as 
GitLab issues in the [application
project](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/ballots-webapp/).

Issues discovered with deployment, e.g. mis-configured certificates, broken authentication, etc,
should be opened as GitLab issues in the [deployment
project](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/infrastructure).

## Environments

The Regent House Ballots application is currently deployed to the following environments with the
following SAML metadata records:

| Name        | URL                | Metadata record |
| ----------- | ------------------ | -- |
| Production  | https://ballots.apps.cam.ac.uk/ | https://metadata.raven.cam.ac.uk/metadata/show/945/ |
| Staging  | https://staging.ballots.apps.cam.ac.uk/ | https://metadata.raven.cam.ac.uk/metadata/show/944/ |
| Development  | https://development.ballots.apps.cam.ac.uk/ | https://metadata.raven.cam.ac.uk/metadata/show/943/ |

## Source code

The source code for the Regent House Ballots application is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Web application](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/ballots-webapp/) | Django-based web application |
| [Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/infrastructure/) | Terraform-based deployment |

These repositories are configured in the [GitLab project
factory](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-project-factory/).

## Technologies used

The following gives an overview of the technologies the Regent House Ballots application is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Web application | Python | Django |
| Deployment | Terraform | Google Cloud Platform |

Special note should be taken that this application uses Raven SAML 2.0 for authentication.
Post-deployment configuration steps are documented in the [deployment project's
README](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/infrastructure#post-deploy-configuration).

## Operational documentation

The following gives an overview of how the Regent House Ballots application is deployed and
maintained.

### Users' guide

The users' guide for the application is maintained in a [Google
doc](https://docs.google.com/document/d/17msoT7ORlBXHDweKMhSCmqporN55HGt-nfO_Zsl1mNU/edit) and is
published to a [public
website](https://docs.google.com/document/d/e/2PACX-1vToIr4wGk9GWT8_zXXoIFnl1sEs9nhlGN7yGgwGiiTRISKJCCRzScTsM22EGQkDfXrYUmdGpd7lIwWZ/pub)
which is linked to from the application itself. This link may be customised in the terraform
configuration.

### Access control

Access control in the application is keyed by Lookup group membership. This is configured in the
deployment project.

> **IMPORTANT** The membership of the uis-regent-house-ballot-{...} groups must be "University" or
> "World" in order for Raven SAML 2 to pass membership information to the application.

In **development**, any member of
[uis-regent-house-ballot-superadmins](https://www.lookup.cam.ac.uk/group/uis-regent-house-ballot-superadmins)
has both staff and superuser status.

In **production** and **staging**, members of
[uis-regent-house-ballot-superadmins](https://www.lookup.cam.ac.uk/group/uis-regent-house-ballot-superadmins)
have both staff and superuser status. Members of
[uis-regent-house-ballot-administrators](https://www.lookup.cam.ac.uk/group/uis-regent-house-ballot-administrators)
have staff and ballot manager status.

In production and staging, it is envisaged that technical support staff will be members of
uis-regent-house-ballot-superadmins while "normal" ballot administrators will be members of
uis-regent-house-ballot-administrators.

### How and where the Regent House Ballots application is deployed

Deployment is via our standard [terraform deployment CI
pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/terraform-pipeline.yml?ref_type=heads).

### Deploying a new release

Deployment is done by:

1. **If necessary**, creating a new release in the application project by creating a git tag which
   matches the new version number.
2. Updating the deployment project's repository with any changes.
3. Using the "play" buttons in the CI pipeline to deploy to production when happy. (Deployments to
   staging happen automatically on merges to `main`.)

### Monitoring

Monitoring is configured as per our standard Google Cloud Run application module.

## Service management and tech lead

The **service owner** for the Regent House Ballots application is [Ceri
Benton](https://www.lookup.cam.ac.uk/person/crsid/cb765).

The **service manager** for the Regent House Ballots application is [Andrew
Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322).

The **tech lead** for the Regent House Ballots application is **TBA**.

The following engineers have operational experience with the Regent House Ballots application and are able to
respond to support requests or incidents:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

Contacts at the University's voting provider are included in the [infrastructure project's
README](https://gitlab.developers.cam.ac.uk/uis/devops/regent-house-ballots/infrastructure/).
