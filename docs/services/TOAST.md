# TOAST

!!! warning
    This page incomplete. Please contact [uis-cloud@lists.cam.ac.uk](mailto:uis-cloud@lists.cam.ac.uk) for more information

This page gives an overview of the TOAST, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

TOAST is used for graduate funding scoring system, plus portal for Gates Trust to manage scholars.

## Service Status

The TOAST is currently **live**.

<!-- `<notes about status and links to roadmaps / timelines for future development or decommissioning>` -->

## Contact

Technical queries and support should be directed to <mailto:uis-cloud@lists.cam.ac.uk> and will be picked up
by a member of the team working on the service.

<!-- Issues discovered in the service or new feature requests should be opened as 
`[GitLab issues in the application repository](<link to gitlab issues page>)`.
-->
## Environments

The TOAST is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | TBA                | TBA             |
| Staging     | TBA                | TBA             |
| Development | TBA                | TBA             |

<!-- `<notes about access to environments and anything special about how an environment is used>` -->

<!-- ## Source code

The source code for the TOAST is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server]() | The source code for the main application server |
| [Infrastructure Deployment]() | The Terraform infrastructure code for deploying the application server to GCP |
-->

## Technologies used

The following gives an overview of the technologies the TOAST is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | VB.NET | |
| Client | VB.NET | |

<!-- ## Operational documentation

The following gives an overview of how the TOAST is deployed and maintained.

### How and where the TOAST is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and locally>`

### `<other operation issues>`

`<documentation or links to external docs about other operation issues>`
-->

## Service Management and tech lead

The **service owner** for the TOAST is **TBA**

The **service manager** for the TOAST is **TBA**

The **tech lead** for the TOAST is [Martin Hunt](https://www.lookup.cam.ac.uk/person/crsid/meh37)
<!--
The following engineers have operational experience with the TOAST and are able to
respond to support requests or incidents:

* `<engineer name and Lookup link>`
* `<engineer name and Lookup link>`
-->