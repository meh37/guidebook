# Yuja Enterprise Video Platform

This page gives an overview of the Yuja Enterprise Video Platform, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description
The Yuja Enterprise Video Platform (Yuja) provides a video and audio transcoding and hosting service.

## Service Status
Yuja is currently in beta testing.

## Contact

Technical queries and support should be directed to [helpdesk@uis.cam.ac.uk](mailto:helpdesk@uis.cam.ac.uk) and will be picked up by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [helpdesk@uis.cam.ac.uk](mailto:helpdesk@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/sms/yuja-admin/-/issues).

Documentation errors can be sent to [support@yuja.com](mailto:support@yuja.com)
## Environments

Yuja is currently deployed to the following environments:

| Name        | URL                                  | Supporting VMs                                     |
| ----------- | ------------------------------------ | -------------------------------------------------- |
| Production  | [https://media.cam.ac.uk](https://media.cam.ac.uk) | None |
| Staging     | [https://cam.yuja.com](https://cam.yuja.com) | None |

## Source code

No source code repository is available, Yuja is cloud based SaaS.


## Technologies used

Yuja is SaaS, which uses AWS Cloud services.

## Operational documentation

The following gives an overview of how Yuja is deployed and maintained.

### How and where the Streaming Media Service is deployed

Yuja is cloud based SaaS, deplyment is managed by Yuja.

### Monitoring
 TBC

### Backups

Yuja is cloud based SaaS, backups are managed by Yuja.

### Other Documentation
- [Main Yuja page](https://www.yuja.com/)
- [Yuja API documentation](https://support.yuja.com/hc/en-us/articles/360049580714-YuJa-API#1-1-user-object-model-0-7)
- [Gitlab issues page](https://gitlab.developers.cam.ac.uk/uis/devops/sms/yuja-admin/-/issues)

## Service Management and tech lead

The **service owner** for Yuja is [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203).

The **service manager** for Yuja is [Anna Langley](https://www.lookup.cam.ac.uk/person/crsid/jal58)

The **tech lead** for Yuja is TBC.

System Dependencies
-------------------
- [Lookup](lookup.md)
- [Raven](raven-ucamwebauth.md)

Contact Addresses
-----------------
- helpdesk@uis.cam.ac.uk
- support@yuja.com