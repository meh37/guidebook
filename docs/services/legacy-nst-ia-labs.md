# Legacy NST IA Lab Allocation

This page gives an overview of Legacy NST IA Lab Allocation, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This system is responsible for supporting the annual process of allocating students to
Laboratory Practical groups for the Natural Sciences Tripos, Part IA (NST IA).

The overall process is overseen by the [NST Co-ordinator](mailto:natsci@admin.cam.ac.uk).

There are three main phases:

* Registration (College staff enter Student choices in September/early October)
* Allocation (Start of Michaelmas term)
* Publishing (Allocation results available online)

During the rest of the year, there is also behind-the-scenes work to manage any proposed
changes to the timetable, and to configure the system.

Registration and Publishing are handled by an external-facing web interface.

Allocation is handled internally by UIS staff.

Public-facing information for [Information for College Staff](https://www.natsci.tripos.cam.ac.uk/staff/practical-allocation).


## Service Status

Legacy NST IA Lab Allocation is currently live, and plans are underway
to replace the system in due course.


## Contact

Students should contact College and Departmental staff in the first instance.

Staff should contact the [NST Co-ordinator](mailto:natsci@admin.cam.ac.uk).

UIS Staff should refer to
[Confluence](https://confluence.uis.cam.ac.uk/display/N1PA/DevOps+Division+Guidebook+-+Reference+Pages).

The Registration and Publishing servers (see below) are overseen by
[UIS Servers and Storage Team (infra-sas)](https://confluence.uis.cam.ac.uk/display/ISC/Infrastructure+Support+Contacts).


## Environments

Legacy NST IA Lab Allocation is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| *REGISTRATION+PUBLISHING:*  |    |                 |
| Production  | https://nstreg.admin.cam.ac.uk/robots.txt<br/>Note there are no world-visible UI pages. | nstreg-live1.internal.admin.cam.ac.uk<br/> nstreg-stby1.internal.admin.cam.ac.uk (Hot standby) |
|             | Database | webdb3.internal.admin.cam.ac.uk/nstreg_live |
| Staging     | https://nstreg-staging.admin.cam.ac.uk/robots.txt<br/>Note there are no world-visible UI pages. | nstreg-staging1.internal.admin.cam.ac.uk |
|             | Database | webdb3.internal.admin.cam.ac.uk/nstreg_staging |
| *ALLOCATION:*   |                | AWS: see [Confluence](https://confluence.uis.cam.ac.uk/display/N1PA/DevOps+Division+Guidebook+-+Reference+Pages). |



## Technologies used

The following gives an overview of the technologies Legacy NST IA Lab Allocation is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | Java | Spring, Hibernate |


## Further Information

For further information relevant to UIS staff, please see
[Confluence](https://confluence.uis.cam.ac.uk/display/N1PA/DevOps+Division+Guidebook+-+Reference+Pages).




## Service Management and tech lead

The **service owner** for Legacy NST IA Lab Allocation is TBD.

The **service manager** for Legacy NST IA Lab Allocation is TBD.

The **tech lead** for Legacy NST IA Lab Allocation is: see
[Confluence](https://confluence.uis.cam.ac.uk/display/N1PA/DevOps+Division+Guidebook+-+Reference+Pages).

The following engineers have operational experience with Legacy NST IA Lab Allocation and are able to
respond to support requests or incidents: see
[Confluence](https://confluence.uis.cam.ac.uk/display/N1PA/DevOps+Division+Guidebook+-+Reference+Pages).
