# Research Dashboard

This page gives an overview of the Research Dashboard, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

This service allows those involved in research administration to view:

- Grant finances (summary expenditure, grant balances and team details)
- Application deadlines
- Contract status and progress updates

## Service Status

The Research Dashboard is currently beta.

## Contact

Technical queries and support should be directed to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to [devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk) rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp/-/issues).

## Environments

The Self-Service Gateway is currently deployed to the following environments:

| Name        | Main Application URL | Django Admin URL | Backend API URL |
| ----------- | -------------------- | ---------------- | --------------- |
| Production  | [https://researchdashboard.admin.cam.ac.uk/](https://researchdashboard.admin.cam.ac.uk/) | [https://researchdashboard.admin.cam.ac.uk/admin](https://researchdashboard.admin.cam.ac.uk/admin) | [https://researchdashboard.admin.cam.ac.uk/api/](https://researchdashboard.admin.cam.ac.uk/api/) |
| Staging  | [https://test.researchdashboard.admin.cam.ac.uk/](https://test.researchdashboard.admin.cam.ac.uk/) | [https://test.researchdashboard.admin.cam.ac.uk/admin](https://test.researchdashboard.admin.cam.ac.uk/admin) | [https://test.researchdashboard.admin.cam.ac.uk/api/](https://test.researchdashboard.admin.cam.ac.uk/api/) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Main Application Hosting | Database |
| ----------- | ------------------------ | -------- |
| Production | [Cloud Run (Project: rd-prod-e68d2d52)](https://console.cloud.google.com/run?project=rd-prod-e68d2d52) | [GCP Cloud SQL (DB name: sql-bbb6ecf4)](https://console.cloud.google.com/sql/instances/sql-bbb6ecf4/overview?project=rd-prod-e68d2d52) |
| Staging | [Cloud Run (Project: rd-test-692a8e82)](https://console.cloud.google.com/run?project=rd-test-692a8e82) | [GCP Cloud SQL (DB name: sql-f64dd06d)](https://console.cloud.google.com/sql/instances/sql-f64dd06d/overview?project=rd-test-692a8e82) |
| Development | [Cloud Run (Project: rd-devel-2d49ecb5)](https://console.cloud.google.com/run?project=rd-devel-2d49ecb5) | [GCP Cloud SQL (DB name: sql-722570e9)](https://console.cloud.google.com/sql/instances/sql-722570e9/overview?project=rd-devel-2d49ecb5) |
## Source code

The source code for the Research Dashboard is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp) | The source code for the main application server. |
| [CUFS GMS Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/datamart-api) | The source code for the micro-service querying CUFS grant management system data. |
| [CHRIS Grant Contracts Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/chris-salaries-api) | The source code for the micro-service querying CHRIS contracts data. |
| [CUFS Salary Commitments Calculator](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/salaries-commitments) | The source code for a salaries commitments calculator for CUFS. |
| [Staff oncost calculator](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/ucamstaffoncosts) | The source code for a python module to calculate total staff oncosts. |
| [School Data Cache](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/school-data-cache) | The source code for caching of university school metadata. |
-| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/infrastructure) | The Terraform infrastructure code for deploying the application server to GCP |

## Technologies used

The following gives an overview of the technologies the Research Dashboard is built on.

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python | Django |
| Client | JavaScript | jQuery |
| Client | TypeScript | React |

## Operational documentation

The following gives an overview of how the Research Dashboard is deployed and maintained.

### How and where the Self-Service Gateway is deployed

Database for application data is a PostgreSQL database hosted by GCP Cloud SQL.
The main web application is a Django application, hosted by GCP Cloud Run.

### Deploying a new release

The README.md file in the Infrastructure Deployment repository explains how to deploy the Research Dashboard.

### Monitoring

Production Uptime and SSL check alerts are being sent to `devops-alarms@uis.cam.ac.uk`.
To see the Monitoring dashboard go to
[GCP Console](https://console.cloud.google.com/monitoring?project=rd-meta-494d1ba5). 

### Debugging

See monitoring.

### Administration

Any operational or administration issues should be raised as an issue in [a repository for this purpose](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/operations/-/issues).

## Service Management and tech lead

The **service owner** for the Research Dashboard is [Dawn Edwards](https://www.lookup.cam.ac.uk/person/crsid/dgb26).

The **service managers** for the Research Dashboard are
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203) and
[Will Russell](https://www.lookup.cam.ac.uk/person/crsid/weer2).

The **tech lead** for the Research Dashboard is [Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174).

The following engineers have operational experience with the Research Dashboard and are able to
respond to support requests or incidents:

* [Wajdi Hajji](https://www.lookup.cam.ac.uk/person/crsid/wh330)
