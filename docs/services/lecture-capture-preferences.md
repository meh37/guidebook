# Lecture Capture Preferences App

This page documents key information about the Lecture Capture Preferences App
service.

!!! warning

    This application is in deep maintainance mode. Only vital fixes should be
    made.

The Lecture Capture Preferences app allows any member of the University with a
Raven account to express a preference surrounding the recording of any lectures
they may give.

It is unclear if this application is being used with the current lecture capture
service.

## Environments

- Production at https://preferences.lecturecapture.uis.cam.ac.uk/
    - API at https://preferences.lecturecapture.uis.cam.ac.uk/api/preferences/?user=CRSID
- Staging at https://webapp.test.lc.gcp.uis.cam.ac.uk/
- Development at https://webapp.devel.lc.gcp.uis.cam.ac.uk/

## Application repositories

- [Web
    application](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/preferences-webapp/)
- [Terraform
    deployment](https://gitlab.developers.cam.ac.uk/uis/devops/lecture-capture/deploy)

## Deployment

Deployed via terraform using our usual deployment boilerplate.

## Current Status

Beta - pending review of utility

## Service Management and tech lead

The service owner for the service is currently unknown.

The **service manager** for the service is [Abraham
Martin](https://www.lookup.cam.ac.uk/person/crsid/rjw57).

The **tech lead** for the service is vacant.

The following engineers have operational experience are able to respond to support requests or incidents:

* [Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)
* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg11)
