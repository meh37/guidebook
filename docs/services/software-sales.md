# Software Sales

!!! danger
    This service has been retired and has been migrated to [https://help.uis.cam.ac.uk/service/software](https://help.uis.cam.ac.uk/service/software)

This page documents key information about the Software Sales service. This service is being decomissioned

## Environments and Servers they run on
- [Production](https://software-old.srv.uis.cam.ac.uk/)
- [Old Production URL - Now redirect to UIS Page](https://software.uis.cam.ac.uk/)

## Application repositories
- [Application](https://gitlab.developers.cam.ac.uk/uis/devops/software-sales/software-sales)

## Technology
| Category | Language    | Framework(s) |
| -------- | ----------- | ------------ |
| Server   | Java+Groovy | Grails       |
| DB       | Postgres    | [Moa](https://confluence.uis.cam.ac.uk/display/SYS/Moa%3A+managed+database+service) |

## Deployment
Manually deployed

## Environments
| Name        | URL                                           | Supporting VMs    |
| ----------- | --------------------------------------------- | ----------------- |
| Production  | https://software-old.srv.uis.cam.ac.uk/       | reacher           |
|             |                                               | rutherer          |
| Test        |                                               | jabberer          |

## Service Owner
UIS Finance

## Current Status
Retired
