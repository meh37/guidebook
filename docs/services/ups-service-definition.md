# University Payment System

This page gives an overview of the University Payment System, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

UPS allows individuals within the organisation to make a one off payment to people. Once approved by finance, payments are loaded into CHRIS and paid via the payroll module.

## Service Status

The University Payment System is currently generally available and is live . The Health of the application is AT RISK, as Framework  is out of date, Can't be updated without a full rewrite.

## Contact

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/university-payment-system-ups/ups).

## Environments

The University Payment System is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  |[https://hrsystems.admin.cam.ac.uk/ups](https://hrsystems.admin.cam.ac.uk/ups) |silver.internal.admin.cam.ac.uk<br />ellington.internal.admin.cam.ac.uk |
| Staging     |[https://staging.hrsystems.admin.cam.ac.uk/ups](https://staging.hrsystems.admin.cam.ac.uk/ups) | jackson.internal.admin.cam.ac.uk |
|Testing      |[https://testing.hrsystems.admin.cam.ac.uk/ups](https://testing.hrsystems.admin.cam.ac.uk/ups) | roach.internal.admin.cam.ac.uk |



## Source code

The source code for the University Payment System is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server](https://gitlab.developers.cam.ac.uk/uis/devops/university-payment-system-ups/ups.git) |  The source code for the main application server |



## Technologies used

The following gives an overview of the technologies the University Payment System is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
|Application  | Java 8 |Play Framework 1.5, Hibernate 5.2.10, Lucene 5.3, Apache Ivy 2.4.0  |

## Operational documentation

The following gives an overview of how the University Payment System is deployed and maintained.

### How and where the University Payment System is deployed

Play! Framework can automatically generate a WAR file for deployment. Batch files (see below) have been created to automate this process.

#### Development

- Execute `{Project Working Copy}/build/create_war_dev.bat`
- Output is to `{Project Working Copy}/build/dist/ups_dev.war`

#### UAT (Play)

- Execute `{Project Working Copy}/build/create_war_uat.bat`
- Output is to `{Project Working Copy}/build/dist/ups_uat.war`

#### Live

- Execute `{Project Working Copy}/build/create_war_prod.bat`
- Output is to `{Project Working Copy}/build/dist/ups_prod.war`

### Deploying a new release

#### Application Servers

The hrutils application is deployed to the same tomcat instance as UPS.

#### Application Server Scripts

Given that this app is relatively small and usage is light the application is deployed to the UPS tomcat and therefore the UPS scripts can be used to stop, start and deploy this app.

#### How To Deploy WAR (Post-Build)

!!! info
    The batch file mentioned below (`push_war_dev_???.bat`) assume an SSH key file exists for authentication (see `REMOTE_PASSWORD_PARAM` environment variable). Change the value as appropriate.


##### Development

Execute `{Project Working Copy}/build/push_war_dev_???.bat`. This copies `ups_dev.war` to `/apps/ups/tomcat/webapps` (on hoy/hunter)

##### UAT (Play)
Execute `{Project Working Copy}/build/push_war_uat_???.bat`. This copies ups_uat.war to `/apps/ups/tomcat/webapps` (on hogwood/hendricks)

##### Live
Execute `{Project Working Copy}/build/push_war_prod_???.bat`. This copies ups_live.war to `/apps/ups/tomcat/webapps` (on hampton/hume)```

### Monitoring

#### Load Balancer

Round-robin load balancing of HTTP traffic with sticky sessions (stickiness controlled by load balancer). SSL offloading is also performed by the load balancer.

HTTP Headers set (for use by application). See Wikipedia for a full list of HTTP Headers.

`X-Forwarded-Proto` - a de facto standard for identifying the originating protocol of an HTTP request, since a reverse proxy (load balancer) may communicate with a web server using HTTP even if the request to the reverse proxy is HTTPS.

`X-Forwarded-For` - a de facto standard for identifying the originating IP address of a client connecting to a web server through an HTTP proxy or load balancer.

| Database Servers | JDBC Connection String | DB User|
| -------- | -------- | --------- |
|Development |Not available at the moment| chris_utils|
|UAT|N/A|chris_utils|
|Live|jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP) (HOST = ocm.internal.admin.cam.ac.uk) (PORT = 1531)) (CONNECT_DATA = (SERVICE_NAME = HR_LIVE)))|chris_utils|


## Service Management and tech lead

The **primary user** for the University Payment System is [Alexsis Dicken](https://www.lookup.cam.ac.uk/person/crsid/aan29)  

The **service owner** for the University Payment System is [Simon Virr](https://www.lookup.cam.ac.uk/person/crsid/sav25).

The **service manager** for the University Payment System is [Andrew Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322).

The **tech lead** for the University Payment System is [Simon Redhead](https://www.lookup.cam.ac.uk/person/crsid/snr21).

The following engineers have operational experience with the University Payment System and are able to
respond to support requests or incidents:

* [Abubakar Zubair](https://www.lookup.cam.ac.uk/person/crsid/az330)
