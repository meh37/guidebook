# `<service_name>`

This page gives an overview of the `<service_name>`, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

## Service Description

`<high level description of what the service does>`

## Service Status

The `<service_name>` is currently `pre-alpha|alpha|beta|live|decommissioning`.

`<notes about status and links to roadmaps / timelines for future development or decommissioning>`

## Contact

Technical queries and support should be directed to `<role email>` and will be picked up
by a member of the team working on the service. To ensure that you receive a response, always
direct requests to `<role email>` rather than reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as 
`[GitLab issues in the application repository](<link to gitlab issues page>)`.

## Environments

The `<service_name>` is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | `<production_url>` | `<list of vms>` |
| Staging     | `<staging_url>`    | `<list of vms>` |
| Development | `<dev_url>`        | `<list of vms>` |

`<notes about access to environments and anything special about how an environment is used>`

## Source code

The source code for the `<service_name>` is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Application Server]() | The source code for the main application server |
| [Infrastructure Deployment]() | The Terraform infrastructure code for deploying the application server to GCP |


## Technologies used

The following gives an overview of the technologies the `<service_name>` is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Server | {name and version} | {name and version} |
| Client | {name and version} | {name and version} |
| {other} | {name and version} | {name and version} |


## Operational documentation

The following gives an overview of how the `<service_name>` is deployed and maintained.

### How and where the `<service_name>` is deployed

`<documentation or links to external docs describing how and where the application is deployed>`

### Deploying a new release

`<documentation or links to external docs about how a new release is made and deployed>`

### Monitoring

`<documentation or links to external docs about monitoring and how alerts should be responded to>`

### Debugging

`<documentation or links to external docs describing how to debug both a deployed instance and locally>`

### `<other operation issues>`

`<documentation or links to external docs about other operation issues>`


## Service Management and tech lead

The **service owner** for the `<service_name>` is `<service owner name and Lookup link>`.

The **service manager** for the `<service_name>` is `<service manager name and Lookup link>`.

The **tech lead** for the `<service_name>` is `<tech lead name and Lookup link>`.

The following engineers have operational experience with the `<service_name>` and are able to
respond to support requests or incidents:

* `<engineer name and Lookup link>`
* `<engineer name and Lookup link>`
