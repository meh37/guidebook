# Time Allocation Survey

This page gives an overview of the Time Allocation Survey, describing its current status, where and how it's developed and deployed, and who is responsible for maintaining it.

## Service Description
The University undertakes an annual Time Allocation Survey (TAS) which involves asking University academic staff whose duties include Teaching and Research to estimate the time they spend on these and other activities. The purpose of the TAS is to attribute costs to these activities.  

The results are then used (in conjunction with other cost drivers) to ascertain the costs of Teaching, Research and Other activity by the University and to calculate the University’s Research fEC rates.

## Service Status

The Time Allocation Survey is currently live.

## Contact

Issues discovered in the service or new feature requests should be opened as 
[GitLab issues in the application repository](https://gitlab.developers.cam.ac.uk/uis/devops/time-allocation-survey).

## Environments

The Time Allocation Survey is currently deployed to the following environments:

| Name        | URL                | Supporting VMs  |
| ----------- | ------------------ | --------------- |
| Production  | [https://tas.admin.cam.ac.uk/tas-web](https://tas.admin.cam.ac.uk/tas-web) <br>[https://tas.admin.cam.ac.uk/tas-cms-web/](https://tas.admin.cam.ac.uk/tas-cms-web/)<br>[https://tas.admin.cam.ac.uk/tas-web-questionnaire/login/do](https://tas.admin.cam.ac.uk/tas-web-questionnaire/login/do) | sanders (primary), timmons (failover) |
| Staging     | [https://staging.app3.admin.cam.ac.uk/tas-web/](https://staging.app3.admin.cam.ac.uk/tas-web)<br>[http://staging.app3.admin.cam.ac.uk/tas-cms-web/login/home](http://staging.app3.admin.cam.ac.uk/tas-cms-web/login/home)<br>[http://staging.app3.admin.cam.ac.uk/tas-web-questionnaire/login/home](http://staging.app3.admin.cam.ac.uk/tas-web-questionnaire/login/home)| jackson.internal.admin.cam.ac.uk |
| Testing |[https://staging.app2.admin.cam.ac.uk/tas-web/login/do](https://staging.app2.admin.cam.ac.uk/tas-web/login/do)<br>[http://staging.app2.admin.cam.ac.uk/tas-cms-web/login/home](http://staging.app2.admin.cam.ac.uk/tas-cms-web/login/home)<br>[http://staging.app2.admin.cam.ac.uk/tas-web-questionnaire/login/home](http://staging.app2.admin.cam.ac.uk/tas-web-questionnaire/login/home) | roach.internal.admin.cam.ac.uk |


## Source code

The source code for the Time Allocation Survey is in Gitlab repository below:

| Repository  | Description
| ----------- | ------------------ |
| [Application](https://gitlab.developers.cam.ac.uk/uis/devops/time-allocation-survey) | The source code for the main application server | 


## Technologies used

The following gives an overview of the technologies the `<service_name>` is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Application | Java | Springframe |




## Operational documentation

The following gives an overview of how the Time Allocation Survey is deployed and maintained.

### How and where the Time Allocation Survey is deployed

TDB

### Deploying a new release

TDB




## Service Management and tech lead

The **service owner** for the Time Allocation Survey is [Arianne Abouzar](https://www.lookup.cam.ac.uk/person/crsid/apa36).

The **service manager** for the Time Allocation Survey is [Andrew Crook](https://www.lookup.cam.ac.uk/person/crsid/ajc322)

The **tech lead** for the Time Allocation Survey is [Simon Redhead](https://www.lookup.cam.ac.uk/person/crsid/snr21)

The following engineers have operational experience with the Time Allocation Survey and are able to
respond to support requests or incidents:

* [Abubakar Zubair](https://www.lookup.cam.ac.uk/person/crsid/az330)
* [Rekha Rajan](https://www.lookup.cam.ac.uk/person/crsid/rr494)

