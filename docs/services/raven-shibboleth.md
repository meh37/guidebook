title: SAML2

# Raven SAML2

This page gives an overview of the Raven SAML2 service, describing its current
status, where and how it's developed and deployed, and who is responsible for
maintaining it.

## Service Description

The Raven service provides a self-service, web-based interactive sign in service
for the University. It has several parts. Raven SAML2 provides a standard [SAML
2.0](https://en.wikipedia.org/wiki/SAML_2.0) interface for sites around the
University.

There is a [dedicated documentation site](https://docs.raven.cam.ac.uk/) for
Raven including its SAML2 interface.

## Service Status

The Raven SAML2 service is currently live. There are no plans to decommission
the service as we need to run a SAML2 service to operate within the [UK Access
Management Federation](https://www.ukfederation.org.uk/).

## Contact

Technical queries and support should be directed to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) and will be
picked up by a member of the team working on the service. To ensure that you
receive a response, always direct requests to
[raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk) rather than
reaching out to team members directly.

Issues discovered in the service or new feature requests should be opened as
GitLab issues in the appropriate project within the
[Shibboleth group](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/)
or [Raven Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure)
project (both DevOps only).

## Environments

Raven SAML2 is currently deployed to the following environments:

| Name        | Main Application URL | GCP Project      |
| ----------- | -------------------- | ---------------- |
| Production  | [https://shib.raven.cam.ac.uk/](https://shib.raven.cam.ac.uk/) | [Raven Core IdP - production](https://console.cloud.google.com/home/dashboard?project=raven-core-prod-3bdf4baa) |
|             | [https://shibboleth.prod.raven-core.gcp.uis.cam.ac.uk/](https://shibboleth.prod.raven-core.gcp.uis.cam.ac.uk/) | |
| Staging     | [https://shib-test.raven.cam.ac.uk/](https://shib-test.raven.cam.ac.uk/) | [Raven Core IdP - staging](https://console.cloud.google.com/home/dashboard?project=raven-core-test-7167dedf) |
|             | [https://shibboleth.test.raven-core.gcp.uis.cam.ac.uk/](https://shibboleth.test.raven-core.gcp.uis.cam.ac.uk/) | |
| Development | [https://shibboleth.devel.raven-core.gcp.uis.cam.ac.uk/](https://shibboleth.devel.raven-core.gcp.uis.cam.ac.uk/) | [Raven Core IdP - development](https://console.cloud.google.com/home/dashboard?project=raven-core-devel-0d19e404) |

All environments access a meta project
([Raven Core Idp meta](https://console.cloud.google.com/home/dashboard?project=raven-core-meta-71b30c4c))
for shared secrets and monitoring.

!!! tip

    [Public-facing documentation](https://help.uis.cam.ac.uk/service/accounts-passwords/it-staff/testing)
    for testing Raven SAML2 can be found on the UIS webpage.

## Source code

Source code for Raven SAML2 is spread over the following repositories:

| Repository | Description |
| ---------- | ----------- |
| [Shibboleth](https://git.shibboleth.net/view/?p=java-identity-provider.git;a=summary) | External repository holding the Shibboleth source code itself |
| [IdP Frontend Container](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/idp-frontend)<sup>2</sup> | Containerised Apache2 frontend which handles interactive authentication |
| [Shib Idp Container](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/shibboleth-idp)<sup>2</sup> | Containerised Shibboleth |
| [Dev Docker Compose](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/shib-docker-compose)<sup>1</sup> | Docker-compose configuration for local development |
| [Raven Infrastructure](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/)<sup>1</sup> | Terraform configuration for infrastructure and deployment |
| [IdP Resolver Test](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/idp-resolver-test)<sup>2</sup> | Testing of attribute release |
| [Shib Usage Stats](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/idp-usage-stats)<sup>2</sup> | Log analysis and stats production |

<sup>1</sup> DevOps only

<sup>2</sup> GitLab users only

## Technologies used

The following gives an overview of the technologies that Raven SAML2 is built
on.

| Category       | Language                 | Framework(s) |
| -------------- | ------------------------ | ------------ |
| Shibboleth IdP | Java, XML and JavaScript | Many         |
| GCP deployment | Terraform                |              |

## Operational documentation

There is a dedicated [operational
documentation folder](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/tree/master/doc)
in the [infrastructure Gitlab project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/)
(DevOps only).

### How and where the service is deployed

The GCP deployment follows our standard deployment practice for Google cloud
with the exact container versions are specified in the [infrastructure
deployment](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/blob/master/locals.tf#L229)
and so deployment follows a "gitops" model.

The non-production deployments can be used as an alternative to production Raven
SAML2 by means of a change to `/etc/hosts` as documented in the [testing
page](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/infrastructure/-/blob/master/doc/testing.md).

### Monitoring

The monitoring and alerting system is based on [Cloud Monitoring](https://cloud.google.com/monitoring).
Alert policies and metrics can be views in the
[Raven Core IdP meta project](https://console.cloud.google.com/monitoring/alerting?project=raven-core-meta-71b30c4c)
(DevOps only).

Our standard alerts have been configured:

* Service uptime check from various geographic regions
* SSL expiry checks
* Check for excessive k8s storage volume usage
* Check for excessive CPU, memory or disk pressure on nodes
* Check for excessive CPU, memory or storage use by pods

In addition, the GCP deployment has the following monitoring:

* Check that University and UK Federation metadata sources are correctly
  imported according to their refresh schedule.

### Debugging

A full environment may be run locally using the [Dev Docker Compose](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/shibboleth/shib-docker-compose)
project (DevOps only). This allows configuration changes to be debugged locally
without affecting any deployed service.

## Service Management and tech lead

The **service owner** for Raven SAML2 is [Vijay
Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1).

The **service manager** for Raven SAML2 is [Dr Я
Charles](https://www.lookup.cam.ac.uk/person/crsid/rc118).

The **tech lead** for Raven SAML2 is [Robin
Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21).

The following engineers have operational experience with Raven SAML2 and are
able to respond to support requests or incidents:

* [Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)
* [Robin Goodall](https://www.lookup.cam.ac.uk/person/crsid/rjg21)
* [Sam Wenham](https://www.lookup.cam.ac.uk/person/crsid/sdw37)
* [Stephen Ison](https://www.lookup.cam.ac.uk/person/crsid/si202)
