---
title: External documentation
---
# External documentation for Git and GitLab

Good sources of documentation for git include:

- The [git documentation site](https://git-scm.com/doc) which links to free ebooks and videos about
  git.
- Members of UIS have access to LinkedIn Learning which has [many
  videos](https://www.linkedin.com/learning/me?u=2963594) on git.
- GitLab maintain [extensive documentation](https://docs.gitlab.com/) on their platform and on
  [using git in general](https://docs.gitlab.com/ee/topics/git/).
