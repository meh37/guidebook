---
title: Configure your IDE
---

# How to configure your IDE or editor for use with our projects

We use the [poetry tool](https://python-poetry.org/) to manage dependencies in many of our Python
projects. This guide covers how to best configure your editor or IDE to work well with our way of
writing Python packages.

If your preferred editor is not present in the list below, feel free to contribute a section.

## Visual Studio Code

This section covers configuration appropriate for Visual Studio Code (VSCode). It does not attempt
to be comprehensive since fine-scale IDE configuration tends to be personal to each developer.

### Python projects using poetry

This section is applicable to webapps following our standard boilerplate or Python libraries using
poetry for packaging. An indication of this is a `poetry.lock` file in the root of the repository.

VSCode has full support for the poetry tool as part of the standard Python extension. We recommend
installing at least the Python and Docker extensions along with, optionally, the "Pylance", "Flake8"
and "Mypy Type Checker" extensions.

To configure VSCode to use the poetry-created virtualenv:

1. If you have not yet done so, run `poetry install` in the application repository's directory.
2. In VSCode, select **File** > **Open Folder...** from the menu and select the application
   repository's directory.
3. Open the command palette via <kbd>Ctrl</kbd>-<kbd>Shift</kbd>-<kbd>P</kbd>, or
   <kbd>&#x2318;</kbd>-<kbd>Shift</kbd>-<kbd>P</kbd> on a Mac.
4. Type "Python: Select Interpreter" and select the command from the drop-down menu.
5. Select the interpreter marked "poetry".

??? tip "But I like my virtualenvs in the project folder!"

    Running `poetry config virtualenvs.in-project true` will configure poetry to [create virtualenvs
    within the application root
    directory](https://python-poetry.org/docs/configuration/#virtualenvsin-project) in a directory
    called `.venv`. VSCode will discover the virtualenv in `.venv` automatically without explicit
    configuration.

To configure VSCode to run tests:

1. Ensure that you have VSCode configured for poetry as described above.
2. Open the command palette as described above and run the command "Python: Configure Tests".
3. Select "pytest" and then select ". (root directory)".
4. Click the "testing" icon in the sidebar. It is a laboratory flask.
5. Click the "play" icon next to tests to run them.

Read more about [testing Python applications](https://code.visualstudio.com/docs/python/testing) in
the VSCode documentation.
