---
title: Prepare your system
---

# How to prepare your system for development

This guide covers how to prepare your system for running and, optionally, developing our
applications. This guide attempts to be generic; there may be product-specific requirements and so
always check the READMEs if you're starting work on a product for the first time.

## Install software

In order to run and develop our standard web applications, you will need to have the following
installed on your system:

1. **Docker.** On Mac and Windows machines you may find [docker
   desktop](https://www.docker.com/products/docker-desktop/) to be the most useful solution. On
   Linux, follow the [docker install instructions](https://docs.docker.com/engine/install/).
2. **Docker compose.** This comes installed with Docker desktop although you may need to check that
   it is enabled in the Docker desktop settings. For Linux machines, you will need to [install the
   docker compose plugin](https://docs.docker.com/compose/install/linux/).
3. **Git.** GitHub provide a [guide on installing
   git](https://github.com/git-guides/install-git). You will also need to make sure that you can
   [sign in to GitLab](./sign-in-to-gitlab.md).
4. **Python.** (Optional) On Mac machines you can use [homebrew](https://brew.sh/). On
   both Linux and Mac machines you can use the system Python or use solutions such as
   [pyenv](https://github.com/pyenv/pyenv) to install specific versions. We try to support
   the [versions which are currently supported by the Python
   developers](https://devguide.python.org/versions/) but you'll usually have a better time the
   more up-to-date your system Python install is.
5. **Node.** (Optional) On Mac machines you can use homebrew for this. For both Linux and Mac you
   can use a toolchain manager such as [volta](https://docs.volta.sh/guide/getting-started). The
   node website has an [installation guide](https://nodejs.org/en/download/package-manager) for
   other systems or if you prefer to install node directly from the source.

The minimum software you need to have installed to _run_ one of our applications locally is
**docker** and **docker compose**. The optional dependencies are required if you want to make
modifications.

If you intend to _deploy_ applications to our Google Cloud infrastructure, you will also need to
install the [logan tool](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan) via the
following command:

```sh
python3 -m pip install --user logan \
  --index-url https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple
```

If you intend to develop applications it is convenient to have some command-line tools installed.
For Python development, we recommend that you install the "poetry" application for managing
dependencies and local virtual environments and the "poe" task runner:

```sh
python3 -m pip install --user poetry poethepoet[poetry-plugin]
```

For TypeScript development, we recommend that you install the common JavaScript package management
tools:

* [npm installation guide](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
* [yarn installation guide](https://yarnpkg.com/getting-started/install)
* [pnpm installation guide](https://pnpm.io/installation)

In addition to the software above, you will need a good programmers' text editor to be productive.
This is a personal choice. Some people prefer full IDEs such as
[IntelliJ](https://www.jetbrains.com/idea/) or [PyCharm](https://www.jetbrains.com/pycharm/), some
prefer more minimalist editors such as [Sublime Text](https://www.sublimetext.com/) and some prefer
middle-ground editors with plugins for extra functionality such as [neovim](https://neovim.io/) or
[VSCodium](https://vscodium.com/).

## Check that your system is ready

Check that docker is installed correctly:

```console
$ docker run --rm hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

!!! tip

    On Linux machines you may get permissions errors. Make sure that your user is part of the
    `docker` group via `#!sh sudo usermod -a -G docker $USER`. You need to log out and back in for
    changes to take effect.

Check that `docker compose` is installed:

```console
$ docker compose --help

Usage:  docker compose [OPTIONS] COMMAND
...
```

If you have installed logan, check that it is on the current `PATH`:

```console
$ logan --help
usage: logan [-h] [-c CONFIGURATION] [--dry-run] [--quiet] [--image-name IMAGE_NAME] [--nobuild] [--nopull]
             [--workdir WORKDIR] [--writable-workdir] [--workspace WORKSPACE]
             [--terraform-data-volume-name TERRAFORM_DATA_VOLUME_NAME] [--rm-terraform-data-volume] [-T]
             [--docker-run-args DOCKER_RUN_ARGS] [--temp-directory TEMP_DIRECTORY] [-v]
             ...
```

??? question "I'm getting a "command not found" error trying to run logan."

    Packages installed by `pip install --user` place command-line scripts under `$HOME/.local/bin`
    by default. You may need to add that location to the `PATH`. This proces may vary depending on
    the Operating System installed on your machine so ask a friendly dev if you're unsure how to do
    this.

If you have installed poetry and poe, check that they are on the current `PATH`:

```console
$ poetry --version
Poetry (version 1.5.1)
$ poe --version
Poe the poet - version: 0.20.0
```

The exact versions shown may differ depending on when you install these applications.

If you have installed the node package managers, check they they are on the current `PATH`:

```console
$ npm --version
9.8.0
$ yarn --version
1.22.19
$ pnpm --version
8.6.12
```

The exact versions shown may differ depending on when you install these applications.

## Summary

In this guide you learned how to install software required for common development tasks.

## Next steps

* Read [how to create a new web application](./create-a-new-app.md).
