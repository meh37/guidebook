---
title: Add common CI jobs
---

# How to add the common CI pipeline to an existing Python project

The [common CI pipeline
template](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/)
provides a number of common jobs useful for Python development.

This page shows you how to add this pipeline to an existing Python project.

## Add the CI configuration

Update the `.gitlab-ci.yml` file to add the common pipeline:

```yaml title=".gitlab-ci.yml"
include:
  - project: "uis/devops/continuous-delivery/ci-templates"
    file: "/auto-devops/common-pipeline.yml"
    ref: "{replace this with the latest ci-templates repository tag}"

variables:
  # Add this variable if you don't have a Dockerfile in the repository and/or
  # want to disable automatic image building.
  BUILD_DISABLED: "1"
```

!!! tip

    The common pipeline brings in Auto DevOps as a dependency and so you don't
    need to explicitly `include` it.

## Set a version number for your package

Python packages should specify a version number in `setup.py` or
`pyproject.toml`.

### Packages using setuptools

Version numbers are set in the `setup` function's arguments:

```python title="setup.py"
from setuptools import setup

setup(
    name="my-package-name",
    version="1.2.3",
    # etc ...
)
```

To check that a version number is set for projects using `setup.py`:

```console
$ python3 setup.py --version
1.2.3
```

### Packages using poetry

Version numbers are set in the `pyproject.toml` file:

```toml title="pyproject.toml"
[tool.poetry]
name = "my-package-name"
version = "1.2.3"
# etc ...
```

To check that a version number is set for projects using poetry:

```console
$ poetry version --short
1.2.3
```

## Add a changelog

Packages should have a `CHANGELOG.md` file in the root of the repository
providing a summary of changes over time. You can see examples in our other
projects.

## Use tox to run tests

The common pipeline will run tests using `tox` if there is a `tox.ini` file
present. The tests are run using the three most recent Python versions for the
release of the common pipeline you are using.

!!! tip

    See the [common CI pipeline Python-specific
    documentation](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/python.md)
    if you need to run custom toxenvs or have additional requirements to run
    tests.

## Add PyPI publish tokens (optional)

The common pipeline publishes Python packages to the GitLab package registry.

If you also want to publish packages to PyPI, you will need to create API tokens
and set the `PYPI_API_TOKEN` and `TEST_PYPI_API_TOKEN` CI variables in the
project settings. This process is described on [a separate
page](./pypi.md).

## Summary

In this how-to you learned how to configure an existing Python project to make
use of the common CI pipeline for testing and publishing.

## Next steps

* See how to [publish a new release](./publish-a-python-package.md).
* Read the [common CI pipeline Python-specific
  documentation](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/auto-devops/python.md).
