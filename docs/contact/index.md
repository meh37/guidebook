# Contacts and emergencies

First contact should **almost always** be made through the UIS service desk.
Their contact details are available on a [UIS Intranet
page](https://universityofcambridgecloud.sharepoint.com/sites/UIS_Staff_Intranet/SitePages/Incident-management-process.aspx).
If you are not a member of UIS, we re-publish the details below but note that
the intranet site is canonical:

* **To log an incident** contact
    [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).
* **To escalate a ticket or contact the Service Desk Manager** contact
    [servicedeskmanager@uis.cam.ac.uk](mailto:servicedeskmanager@uis.cam.ac.uk).

!!! tip

    If you are a DevOps member wishing to give the service desk a "heads up",
    the correct contact is
    [servicedeskmanager@uis.cam.ac.uk](mailto:servicedeskmanager@uis.cam.ac.uk).

The remaining contact details should ordinarily only be used by the service desk
when escalating an incident. _Please do not attempt to circumvent the process by
emailing us directly._

We have some service-specific contact addresses which should be used when
contacting us about the corresponding service:

* GitLab support requests should be [raised as an
    issue](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/issues) on
    the support project in GitLab. Should you need to email the GitLab
    operations team directly, you can used [gitlab-team@developers.cam.ac.uk](mailto:gitlab-team@developers.cam.ac.uk).
* Email about the legacy git.uis service should be directed to
    [gitmaster@uis.cam.ac.uk](mailto:gitmaster@uis.cam.ac.uk).
* Support requests for Raven should be sent to
    [raven-support@uis.cam.ac.uk](mailto:raven-support@uis.cam.ac.uk). The Raven
    admin team can be contacted directly *in an emergency* via
    [raven-admin@uis.cam.ac.uk](mailto:raven-admin@uis.cam.ac.uk).
* Cloud computing (Amazon, Google, Azure, etc) related issues should be reported
    in the first instance to
    [cloudsupport@uis.cam.ac.uk](mailto:cloudsupport@uis.cam.ac.uk).
    Email threads which the Cloud team should be aware of should be Cc-d to
    [cloud@uis.cam.ac.uk](mailto:cloud@uis.cam.ac.uk)
* G Suite@Cambridge related issues should be reported in the first instance to
    [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).
    Email threads which the G Suite team should be aware of should be Cc-d to
    [gapps-admin@uis.cam.ac.uk](mailto:gapps-admin@uis.cam.ac.uk)
* Lookup issues should be sent to [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).
    Only issues of a *technical* nature should be escalated to
    [servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk).

You can send us GPG encrypted files or email by using our [GPG public
keys](./public-keys.md).

The generic email address for the team is
[devops@uis.cam.ac.uk](mailto:devops@uis.cam.ac.uk).
