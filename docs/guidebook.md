# This guidebook

This guidebook documents what we as a team in DevOps aspire to be, what we
currently do and practices we currently find to be helpful.

It is not intended as a prescriptive document but rather to be *descriptive* of
the general way we do things. If the way we do things and the book disagree, it
is the book which is usually at fault.

Occasionally we find ourselves writing guides to ourselves or to others about
how we do things and learnings from mistakes we've made. The [notes
section](notes/index.md) of the guidebook provides a single place for these
notes to live.

### Updating the guidebook

Anyone with a Raven account may propose an edit to the guide by opening a merge
request on the [guidebook
project](https://gitlab.developers.cam.ac.uk/uis/devops/docs/guidebook/) in the
University [Developers' Hub](https://gitlab.developers.cam.ac.uk/). Each page in
the guidebook has an edit link which takes you directly to the corresponding
page in the guidebook project.

Within the division we view this guidebook as an "external wiki"; it should be
quick to update and anyone should feel empowered to do so.
