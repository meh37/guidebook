FROM python:3.11-alpine

RUN apk update && apk add gcc musl-dev

WORKDIR /mnt
COPY requirements.txt .
RUN pip install -r requirements.txt

CMD ["mkdocs",  "serve", "-a", "0.0.0.0:8000"]
