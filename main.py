"""
Definition of macros used within the guidebook.

"""
import os
import re

import yaml

from identitylib.identifiers import IdentifierSchemes

DATA_DIR = os.path.join(os.path.dirname(__file__), "data")

# Load a static list of current GitLab labels. See data/README.md for how this
# file is generated.
with open(os.path.join(DATA_DIR, "labels.yml")) as fobj:
    GITLAB_LABELS = yaml.safe_load(fobj)

# A set of TDA-approved identifier scopes. Eventually this should be moved into identitylib.
_TDA_APPROVED = {
    "v1.person.identifiers.cam.ac.uk",
    "person.v1.student-records.university.identifiers.cam.ac.uk",
    "person.v1.human-resources.university.identifiers.cam.ac.uk",
}


def define_env(env):
    @env.macro
    def gitlab_label(name):
        """Render a GitLab-style label."""

        try:
            # Try to find a matching label.
            label_dict = [
                label for label in GITLAB_LABELS if label["name"] == name][0]
            bg_colour = label_dict["color"]
            text_colour = label_dict["text_color"]
        except IndexError:
            # We have no label, default to dark grey.
            bg_colour, text_colour = "#444", "#fff"

        style = " ".join(
            [
                f"--label-background-colour: {bg_colour};",
                f"--label-text-colour: {text_colour};",
            ]
        )

        if "::" in name:
            scope, name = name.split("::")
            return (
                f'<span class="gl-label gl-label-scoped" style="{style}">'
                f'<span class="gl-label-text">{scope}</span>'
                f'<span class="gl-label-text-scoped">{name}</span>'
                "</span>"
            )
        else:
            return (
                f'<span class="gl-label" style="{style}">'
                f'<span class="gl-label-text">{name}</span>'
                "</span>"
            )

    env.variables["annotation_icon"] = (
        '<span class="md-annotation__index" style="cursor: inherit;">'
        '<span data-md-annotation-id="1"></span>'
        "</span>"
    )

    @env.macro
    def identifier_table(*, deprecated: bool):
        rows = [
            "| Scope | Common name | TDA |",
            "|-|-|:-:|",
        ]

        schemes = [
            getattr(IdentifierSchemes, a)
            for a in dir(IdentifierSchemes)
            if re.match("^[A-Z][A-Z0-9_]+$", a)
        ]
        schemes.sort(key=lambda s: s.identifier)

        for scheme in schemes:
            if not deprecated:
                rows.append(
                    "".join(
                        [
                            f"| `{scheme.identifier}` | {scheme.common_name} |",
                            '<span style="display: none">yes</span> :material-check: |'
                            if scheme.identifier in _TDA_APPROVED
                            else '<span style="display: none">no</span> |',
                        ]
                    )
                )
            for k, v in scheme.aliases.items():
                if not deprecated and "deprecated" in k:
                    continue
                rows.append(
                    "".join(
                        [
                            f"| `{v}` | {scheme.common_name} |",
                            '<span style="display: none">yes</span> :material-check: |'
                            if v in _TDA_APPROVED
                            else '<span style="display: none">no</span> |',
                        ]
                    )
                )

        return "\n".join(rows)
